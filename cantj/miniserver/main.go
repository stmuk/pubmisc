package main

import (
	"fmt"
	"log"
	"net/http"
)

// Experiment with filling the "Edit metadata" form
// using a stripped down version of the original resp
//
// Currently hard coded to return "Age" and "Country"
// it should take these as params.
//
// curl -v  http://127.0.0.1:2112| jq .
//
// Maybe florence can be modified (temporarily) to talk to
// http://127.0.0.1:2112 rather than the usual chain.
// at the "Edit metadata" point?

func main() {

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// better logging?
		log.Printf("req=%#v\n", r)
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintf(w, resp())

	})

	log.Fatal(http.ListenAndServe(":2112", nil))
}

func resp() string {

	return `
{
  "table_query_result": {
    "service": {
      "tables": [
        {
          "name": "LC1117EW",
          "label": "Sex by age",
          "description": "This dataset provides 2011 Census estimates that classify usual residents by sex, and by age (ages 0 to 15 grouped together, 10 year age groups up to 74 then 75 years and over grouped together). The estimates are as at census day, 27 March 2011.",
          "vars": [
            "Region",
            "Sex",
            "Age"
          ],
          "meta": {
            "contact": {
              "contact_name": "Census Customer Services",
              "contact_email": "census.customerservices@ons.gov.uk",
              "contact_phone": "01329 444 972",
              "contact_website": "https://www.ons.gov.uk/census/censuscustomerservices"
            },
            "census_releases": [
              {
                "census_release_description": "Example release: migration and demography",
                "census_release_number": "1",
                "release_date": "30/03/2013"
              }
            ],
            "dataset_mnemonic2011": "LC1117EW",
            "dataset_population": "All usual residents",
            "dissemination_source": "Census 2011",
            "geographic_coverage": "England and Wales",
            "geographic_variable_mnemonic": "Region",
            "last_updated": "28/01/2015",
            "keywords": [],
            "publications": [],
            "related_datasets": [],
            "release_frequency": "",
            "statistical_unit": {
              "statistical_unit": "People",
              "statistical_unit_description": "People living in England and Wales"
            },
            "unique_url": "",
            "version": "1"
          }
        }
      ]
    }
  },
  "dataset_query_result": {
    "dataset": {
      "label": "ONS 2011 Census 1% Sample Teaching Data",
      "description": "",
      "meta": {
        "source": {
          "contact": {
            "contact_name": "Census Customer Services",
            "contact_email": "census.customerservices@ons.gov.uk",
            "contact_phone": "01329 444 972",
            "contact_website": "https://www.ons.gov.uk/census/censuscustomerservices"
          },
          "licence": "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/",
          "methodology_link": "https://www.ons.gov.uk/census/2011census/2011censusdata/censusmicrodata/microdatateachingfile/microdatauserguide",
          "methodology_statement": "The census is the most complete source of information about the population that we have. This Teaching Dataset is a 1% sample of data from the complete England and Wales population, which includes usual residents, short-term residents and students living away from home during term-time.",
          "national_statistic_certified": "Y"
        }
      },
      "variables": {
        "edges": [
          {
            "node": {
              "name": "Region",
              "meta": {
                "ons_variable": {
                  "variable_description": "The geographic region in which a person lives, derived from the address of their household or communal establishment.",
                  "keywords": [],
                  "statistical_unit": {
                    "statistical_unit": "People",
                    "statistical_unit_desc": "People living in England and Wales"
                  }
                }
              }
            }
          },
          {
            "node": {
              "name": "Sex",
              "meta": {
                "ons_variable": {
                  "variable_description": "The classification of a person as either male or female.",
                  "keywords": [
                    "sex",
                    "gender"
                  ],
                  "statistical_unit": {
                    "statistical_unit": "People",
                    "statistical_unit_desc": "People living in England and Wales"
                  }
                }
              }
            }
          },
          {
            "node": {
              "name": "Age",
              "meta": {
                "ons_variable": {
                  "variable_description": "Age is derived from the date of birth question and is a person's age at their last birthday, at 27 March 2011. Dates of birth that imply an age over 115 are treated as invalid and the person's age is imputed. Infants less than one year old are classified as 0 years of age.",
                  "keywords": [
                    "age",
                    "date of birth"
                  ],
                  "statistical_unit": {
                    "statistical_unit": "People",
                    "statistical_unit_desc": "People living in England and Wales"
                  }
                }
              }
            }
          }
        ]
      }
    }
  }
}
`
}
