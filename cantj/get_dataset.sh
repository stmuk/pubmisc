#!/bin/ksh -x

url=http://localhost:8082/login
token=$(curl -s -d "{\"email\":\"florence@magicroundabout.ons.gov.uk\",\"password\":\"$FLORENCE_WEB_PW\"}" $url)

curl -s -X GET "http://localhost:22000/datasets/$1" -H "accept: application/json" -H "Content-Type: application/json" -H "X-Florence-Token: $token" | jq .
