#!/bin/ksh
docker ps | grep dataset-api | awk '{print $1}' 
docker rm -f cantabular-import-journey_dp-dataset-api_1
cd /home/steve/work/pubmisc/cantj/dp-compose/cantabular-import && docker-compose up -d
cid=$(docker ps | grep dataset-api | awk '{print $1}' )
docker exec -it $cid /bin/bash
